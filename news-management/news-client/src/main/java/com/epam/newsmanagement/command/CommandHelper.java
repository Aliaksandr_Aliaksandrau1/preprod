package com.epam.newsmanagement.command;

import java.util.HashMap;
import java.util.Map;

import com.epam.newsmanagement.command.impl.ChangeLocaleCommand;
import com.epam.newsmanagement.command.impl.DefaultCommandImpl;

public class CommandHelper {
	private Map<CommandEnum, ICommand> commandMap = new HashMap<>();

	private CommandHelper() {
		
		commandMap.put(CommandEnum.DEFAULT_COMMAND, new DefaultCommandImpl());
		commandMap.put(CommandEnum.CHANGE_LOCALE, new ChangeLocaleCommand());
		
	}

	private static class SingletonHolder {
		private final static CommandHelper INSTANCE = new CommandHelper();
	}

	public static CommandHelper getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public ICommand getCommand(String commandName) {
		CommandEnum name = CommandEnum.valueOf(commandName.toUpperCase());
		ICommand command;
		if (null != name) {
			command = commandMap.get(name);
		} else {
			command = commandMap.get(CommandEnum.DEFAULT_COMMAND);
		}
		return command;
	}

}
