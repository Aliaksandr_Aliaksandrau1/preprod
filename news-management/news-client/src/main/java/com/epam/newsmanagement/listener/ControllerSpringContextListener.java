package com.epam.newsmanagement.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Application Lifecycle Listener implementation class
 * ControllerSpringContextListener
 *
 */
@WebListener
public class ControllerSpringContextListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public ControllerSpringContextListener() {
		
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent e) {
		
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent e) {

		ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context-bean.xml");
		e.getServletContext().setAttribute("ctx", ctx);
	}
}
