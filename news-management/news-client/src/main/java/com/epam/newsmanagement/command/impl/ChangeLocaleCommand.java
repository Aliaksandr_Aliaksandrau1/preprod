package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.command.CommandException;
import com.epam.newsmanagement.command.ICommand;

public class ChangeLocaleCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		String loc = "loc";
		
		String locale = request.getParameter(loc);
		
		HttpSession session = request.getSession();
		
		session.setAttribute(loc, locale);
		
		
		return "/index.jsp";
	}

}
