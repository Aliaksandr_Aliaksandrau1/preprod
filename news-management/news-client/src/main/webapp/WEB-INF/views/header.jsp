
<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 



<fmt:setLocale value="${sessionScope.loc}" />
<fmt:setBundle basename="resources.locale" var="loc" />
<fmt:message bundle="${loc}" key="general.message.head" var="head" />



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News management</title>
</head>
<body>
	<h1>${head}</h1>
	
	<a href="client?command=change_locale&loc=ru">RU</a>
	<a href="client?command=change_locale&loc=en">EN</a>
	<hr>
	${sessionScope.loc}
	
	
	
	
</body>
</html>