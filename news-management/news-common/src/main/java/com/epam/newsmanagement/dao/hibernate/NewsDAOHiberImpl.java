package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.PaginationCriteria;
import com.epam.newsmanagement.utility.SearchCriteria;

@Transactional
public class NewsDAOHiberImpl implements NewsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final String HQL_SELECT_ALL_NEWS = "SELECT n FROM News n";
	private static final String HQL_DELETE_NEWS_BY_ID = "DELETE from News where id = :id";
	private static final String HQL_DELETE_LIST_OF_NEWS_BY_ID = "DELETE FROM News n WHERE n.id IN (:list)";

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readAllNews() throws DAOException {

		return sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_NEWS).list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> readEntityListPagination(PaginationCriteria pageCrit) throws DAOException {
	
		int maxItemsOnPage;
		int currentPage;

		if (pageCrit.getMaxItemsOnPage() != null && pageCrit.getMaxItemsOnPage() >= 1) {
			maxItemsOnPage = pageCrit.getMaxItemsOnPage();
		} else {
			maxItemsOnPage = 10; // TODO should read from property file
		}

		if (pageCrit.getCurrentPage() != null && pageCrit.getCurrentPage() >= 1) {
			currentPage = pageCrit.getCurrentPage();
		} else {
			currentPage = 1;
		}

		Query query = sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_NEWS);
		query.setFirstResult((currentPage-1) * maxItemsOnPage);
		query.setMaxResults(maxItemsOnPage);
		
		return query.list();
	}

	@Override
	public News readById(Long id) throws DAOException {
		
		return (News) sessionFactory.getCurrentSession().get(News.class, id);
		
	}

	@Override
	public Long create(News entity) throws DAOException {
		
		return (Long) sessionFactory.getCurrentSession().save(entity);
		
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		
		Query query = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_NEWS_BY_ID);
		query.setParameter("id", id);
		
		return (query.executeUpdate() >= 1);

	}

	@Override
	public boolean update(News entity) throws DAOException {
		
		try {
			News newNews = (News) sessionFactory.getCurrentSession().merge(entity);
			return entity.equals(newNews);
		} catch (HibernateException he) {
			throw new DAOException(he);
		}

	}

	@Override
	public boolean setNewsAuthor(Long newsId, Long authorId) throws DAOException {
		
		throw new DAOException("Operation is not supported");
		
	}

	@Override
	public Long countAllEntities() throws DAOException {
		
		try {
			
			return (Long) sessionFactory.getCurrentSession().createCriteria(News.class)
					.setProjection(Projections.rowCount()).uniqueResult();
			
		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}
	

	@Override
	public boolean deleteListOfEntitiesById(List<Long> idList) throws DAOException {
		
		Query q = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_LIST_OF_NEWS_BY_ID);
		q.setParameterList("list", idList);
		
		return (q.executeUpdate() == idList.size());
	}
	

	// TODO to design this method by the next release
	@Override
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		
		throw new DAOException ("Method wil be designed by the next release");
	}

	// TODO to design this method by the next release
	@Override
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		
		throw new DAOException ("Method wil be designed by the next release");
	}


}
