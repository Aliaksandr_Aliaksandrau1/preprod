package com.epam.newsmanagement.utility;

/**
 * Contains data for pagination
 * 
 * @author Aliaksandr_Aliaksand
 *
 */
public class PaginationCriteria {
	private Integer currentPage;
	private Integer maxItemsOnPage;

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getMaxItemsOnPage() {
		return maxItemsOnPage;
	}

	public void setMaxItemsOnPage(Integer maxItemsOnPage) {
		this.maxItemsOnPage = maxItemsOnPage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentPage == null) ? 0 : currentPage.hashCode());
		result = prime * result + ((maxItemsOnPage == null) ? 0 : maxItemsOnPage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginationCriteria other = (PaginationCriteria) obj;
		if (currentPage == null) {
			if (other.currentPage != null)
				return false;
		} else if (!currentPage.equals(other.currentPage))
			return false;
		if (maxItemsOnPage == null) {
			if (other.maxItemsOnPage != null)
				return false;
		} else if (!maxItemsOnPage.equals(other.maxItemsOnPage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaginationCriteria [currentPage=" + currentPage + ", maxItemsOnPage=" + maxItemsOnPage + "]";
	}
}
