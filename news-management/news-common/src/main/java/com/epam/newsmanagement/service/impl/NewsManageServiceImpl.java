package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManageService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utility.PaginationCriteria;

public class NewsManageServiceImpl implements NewsManageService {
	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;
	@Autowired
	private CommentService commentService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	@Override
	public Long createNews(News news) throws ServiceException {

		return newsService.createNews(news);

	}

	@Override
	public Boolean updateNews(News news) throws ServiceException {

		return newsService.updateNews(news);

	}

	@Override
	public Boolean deleteNews(Long newsId) throws ServiceException {

		return newsService.deleteNews(newsId);
	}

	@Override
	public List<News> readAllNews() throws ServiceException {
		return newsService.readAllNews();
	}

	@Override
	public News readNewsById(Long newsId) throws ServiceException {
		return newsService.readNewsById(newsId);
	}

	@Override
	public List<News> readEntityListPagination(PaginationCriteria pageCrit) throws ServiceException {
		return newsService.readEntityListPagination(pageCrit);
	}

	@Override
	public Long countAllNews() throws ServiceException {
		return newsService.countAllNews();
	}

}
