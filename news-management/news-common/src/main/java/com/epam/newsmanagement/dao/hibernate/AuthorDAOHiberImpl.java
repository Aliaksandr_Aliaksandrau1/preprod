package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.PaginationCriteria;

@Transactional
public class AuthorDAOHiberImpl implements AuthorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final String HQL_SELECT_ALL_AUTHORS = "FROM Author";
	private static final String HQL_DELETE_AUTHOR_BY_ID = "DELETE from Author where id = :id";
	private static final String HQL_DELETE_LIST_OF_AUTHORS_BY_ID = "DELETE FROM Author a WHERE a.id IN (:list)";

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readAllAuthors() throws DAOException {
		
		return sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_AUTHORS).list();
		
	}

	@Override
	public Author readById(Long id) throws DAOException {
		
		return (Author) sessionFactory.getCurrentSession().get(Author.class, id);
		
	}

	@Override
	public Long create(Author entity) throws DAOException {
		
		return (Long) sessionFactory.getCurrentSession().save(entity);
		
	}

	@Override
	public boolean delete(Long id) throws DAOException {
		
		Query query = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_AUTHOR_BY_ID);
		query.setParameter("id", id);
		
		return (query.executeUpdate() >= 1);

	}

	@Override
	public boolean update(Author entity) throws DAOException {
		
		try {
			
			Author newAuthor = (Author) sessionFactory.getCurrentSession().merge(entity);
			
			return entity.equals(newAuthor);
			
		} catch (HibernateException he) {
			
			throw new DAOException(he);
		}
	}

	@Override
	public boolean deleteListOfEntitiesById(List<Long> idList) throws DAOException {
		
		Query q = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_LIST_OF_AUTHORS_BY_ID);
		q.setParameterList("list", idList);
		
		return (q.executeUpdate() == idList.size());
	}
	
	@Override
	public Author readAuthorByNewsId(Long newsId) throws DAOException {

		throw new DAOException ("Operation is not supported"); 

	}

	@Override
	public Long countAllEntities() throws DAOException {
		
		try {

			return (Long) sessionFactory.getCurrentSession().createCriteria(Author.class)
					.setProjection(Projections.rowCount()).uniqueResult();

		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> readEntityListPagination(PaginationCriteria pageCrit) throws DAOException {
		
		int maxItemsOnPage;
		int currentPage;

		if (pageCrit.getMaxItemsOnPage() != null && pageCrit.getMaxItemsOnPage() >= 1) {
			maxItemsOnPage = pageCrit.getMaxItemsOnPage();
		} else {
			maxItemsOnPage = 10; // TODO should read from property file
		}

		if (pageCrit.getCurrentPage() != null && pageCrit.getCurrentPage() >= 1) {
			currentPage = pageCrit.getCurrentPage();
		} else {
			currentPage = 1;
		}

		Query query = sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_AUTHORS);
		query.setFirstResult((currentPage - 1) * maxItemsOnPage);
		query.setMaxResults(maxItemsOnPage);

		return query.list();
	}

	

}
