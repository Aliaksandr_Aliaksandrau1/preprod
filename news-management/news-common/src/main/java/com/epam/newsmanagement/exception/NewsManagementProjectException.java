package com.epam.newsmanagement.exception;

/**
 * The SuperClass for all Exceptions in the NewsManagement app
 * 
 * @author Aliaksandr_Aliaksand
 *
 */
public class NewsManagementProjectException extends Exception {
	
	private static final long serialVersionUID = -3828262673971016179L;

	public NewsManagementProjectException() {

	}

	public NewsManagementProjectException(String message) {
		super(message);
	}

	public NewsManagementProjectException(Throwable cause) {
		super(cause);
	}

	public NewsManagementProjectException(String message, Throwable cause) {
		super(message, cause);
	}
}
