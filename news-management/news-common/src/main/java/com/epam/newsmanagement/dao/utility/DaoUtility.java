package com.epam.newsmanagement.dao.utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DaoUtility {

	private final static Logger LOG = Logger.getRootLogger();

	private DaoUtility() {

	}

	/**
	 * Closes connection, prepared statement and result set
	 * 
	 * @param con
	 *            - connection
	 * @param pst
	 *            - prepared statement
	 * @param rs
	 *            - result set
	 */
	public static void closeCon(Connection con, PreparedStatement pst, ResultSet rs) {

		if (rs != null) {

			try {
				rs.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "ResultSet isn't closed.");
			}
		}

		closeCon(con, pst);

	}

	/**
	 * Closes connection and prepared statement
	 * 
	 * @param con
	 *            - connection
	 * @param pst
	 *            - prepared statement
	 */
	public static void closeCon(Connection con, PreparedStatement pst) {

		closePst(pst);

		if (con != null) {

			try {
				con.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "Connection isn't return to the pool.");
			}
		}

	}

	/**
	 * Closes prepared statement
	 * 
	 * @param pst
	 *            - prepared statement
	 */
	public static void closePst(PreparedStatement pst) {

		if (pst != null) {

			try {
				pst.close();
			} catch (SQLException e) {
				LOG.log(Level.ERROR, "PreparedStatement isn't closed.");
			}
		}

	}

}
