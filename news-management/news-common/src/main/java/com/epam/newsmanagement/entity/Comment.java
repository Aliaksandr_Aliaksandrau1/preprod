package com.epam.newsmanagement.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Representation of Comment entity of the DB
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@Entity
@Table(name = "comments")
@SequenceGenerator(name = "default_gen", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
@AttributeOverride(name = "id", column = @Column(name = "comment_id"))
public class Comment extends AbstractEntity {

	private static final long serialVersionUID = 3195183463597582855L;

	@Column(nullable = false, name = "COMMENT_TEXT", length = 100, columnDefinition = "nvarchar2")
	private String commentText;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false, name = "CREATION_DATE")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date creationDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEWS_ID", nullable = false)
	private News news;
	
	@Column(nullable = false, name = "NEWS_ID", insertable = false, updatable = false)
	private Long newsId;

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [news=" + news + ", commentText=" + commentText + ", creationDate=" + creationDate
				+ ", getId()=" + getId() + "]";
	}

}
