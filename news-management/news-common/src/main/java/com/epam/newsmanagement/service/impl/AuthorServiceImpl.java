package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.utility.ExceptionMessages;
import com.epam.newsmanagement.utility.PaginationCriteria;

public class AuthorServiceImpl implements AuthorService {
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	@Override
	public Author readAuthorById(Long id) throws ServiceException {
		try {

			return authorDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createAuthor(Author author) throws ServiceException {
		try {
			return authorDAO.create(author);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateAuthor(Author author) throws ServiceException {
		try {
			return authorDAO.update(author);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteAuthor(Long id) throws ServiceException {
		try {
			return authorDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Author readAuthorByNewsId(Long id) throws ServiceException {
		try {
			return authorDAO.readAuthorByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Author> readAllAuthors() throws ServiceException {
		try {
			return authorDAO.readAllAuthors();
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteListOfAuthors(List<Long> idList) throws ServiceException {
		try {

			return authorDAO.deleteListOfEntitiesById(idList);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countAllAuthors() throws ServiceException {
		
		try {

			return authorDAO.countAllEntities();

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Author> readEntityListPagination(PaginationCriteria pageCrit) throws ServiceException {

		try {

			return authorDAO.readEntityListPagination(pageCrit);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.AUTHOR_SERVICE_EXC + e, e);
		}
	}

}
