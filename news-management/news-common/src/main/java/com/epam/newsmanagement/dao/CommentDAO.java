package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Provides operations to process data for Comments from the data source
 *
 */
public interface CommentDAO extends BaseDAO<Comment> {

	/**
	 * Receives the list of comments
	 * 
	 * @return the list of comments
	 * @throws DAOException
	 */
	public List<Comment> readAll() throws DAOException;

	/**
	 * Deletes data about Comments in the data source
	 * 
	 * @param commentIdList
	 *            the list of the comments identifiers
	 * @return true if all comments were deleted
	 * @throws DAOException
	 */
	public Boolean deleteCommentsById(List<Long> commentIdList) throws DAOException;

	/**
	 * Receives the list of comments selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return the list of comments
	 * @throws DAOException
	 */
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException;

	/**
	 * Deletes comments from DB by news ID
	 * 
	 * @param newsID
	 * @return true if all comments were deleted
	 * @throws DAOException
	 */
	public Boolean deleteCommentsByNewsId(Long newsId) throws DAOException;

}
