package com.epam.newsmanagement.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.epam.newsmanagement.utility.CustomJsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Representation of News entity of the DB
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@Entity
@Table(name = "NEWS")
@SequenceGenerator(name = "default_gen", sequenceName = "NEWS_SEQ", allocationSize = 1)
@AttributeOverride(name = "id", column = @Column(name = "NEWS_ID"))
public class News extends AbstractEntity {
	
	private static final long serialVersionUID = 4763156381870642519L;
	
	@Column(name = "TITLE", nullable = false, length = 100, columnDefinition = "nvarchar2")
	private String title;
	@Column(name = "SHORT_TEXT", nullable = false, length = 300, columnDefinition = "nvarchar2")
	private String shortText;
	@Column(name = "FULL_TEXT", nullable = false, length = 2000, columnDefinition = "nvarchar2")
	private String fullText;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE", nullable = true)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFICATION_DATE", nullable = true)
	private Date modificationDate;

	@OrderBy("tagName")
	@ManyToMany(fetch = FetchType.EAGER, targetEntity = Tag.class)
	@JoinTable(name = "NEWS_TAGS", joinColumns = @JoinColumn(name = "NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	private Set<Tag> tags;

	@OrderBy("authorName")
	@ManyToMany(fetch = FetchType.EAGER, targetEntity = Author.class)
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = @JoinColumn(name = "NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
	private Set<Author> authors;

	@OrderBy("creationDate")
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, targetEntity = Comment.class)
	@JoinTable(name = "COMMENTS", joinColumns = {
			@JoinColumn(name = "NEWS_ID", referencedColumnName = "NEWS_ID") }, inverseJoinColumns = {
					@JoinColumn(name = "COMMENT_ID", referencedColumnName = "COMMENT_ID", unique = true) })
	private List<Comment> comments;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	@JsonSerialize(using = CustomJsonDateSerializer.class)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@JsonSerialize(using = CustomJsonDateSerializer.class)
	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Set<Tag> getTag() {
		return tags;
	}

	public void setTag(Set<Tag> tag) {
		this.tags = tag;
	}

	public Set<Author> getAuthor() {
		return authors;
	}

	public void setAuthor(Set<Author> author) {
		this.authors = author;
	}

	public List<Comment> getComment() {
		return comments;
	}

	public void setComment(List<Comment> comment) {
		this.comments = comment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [title=" + title + ", shortText=" + shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate + ", getId()=" + getId() + "]";
	}
}
