package com.epam.newsmanagement.exception;

/**
 * Exception for Service layer
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
public class ServiceException extends NewsManagementProjectException {

	private static final long serialVersionUID = 83145838788728430L;

	public ServiceException() {

	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
