package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.PaginationCriteria;

@Transactional
public class CommentDAOHiberImpl implements CommentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final String HQL_SELECT_ALL_COMMENTS = "SELECT c FROM Comment c";
	private static final String HQL_DELETE_COMMENT_BY_ID = "DELETE from Comment where id = :id";

	@Override
	public Comment readById(Long id) throws DAOException {
		
		return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
		
	}

	@Override
	public Long create(Comment entity) throws DAOException {
		
		return (Long) sessionFactory.getCurrentSession().save(entity);
		
	}

	@Override
	public boolean delete(Long id) throws DAOException {
	
		boolean deleted = false;
		Query query = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_COMMENT_BY_ID);
		query.setParameter("id", id);
	
		if (query.executeUpdate() != 1) {
			throw new DAOException(String.format("the comment with id = %d was not deleted", id));
		} else {
			deleted = true;
		}
	
		return deleted;
	}

	@Override
	public boolean update(Comment entity) throws DAOException {
	
		Comment newComment = (Comment) sessionFactory.getCurrentSession().merge(entity);
		
		return entity.equals(newComment);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> readAll() throws DAOException {
	
		return sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_COMMENTS).list();
		
	}

	@Override
	public Boolean deleteCommentsById(List<Long> commentIdList) throws DAOException {
	
		throw new DAOException ("Operation is not supported");
		
	}

	@Override
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException {
		
		throw new DAOException ("Operation is not supported");
		
	}

	@Override
	public Boolean deleteCommentsByNewsId(Long newsId) throws DAOException {
		
		throw new DAOException ("Operation is not supported");
		
	}

	@Override
	public boolean deleteListOfEntitiesById(List<Long> idList) throws DAOException {
		
		throw new DAOException ("Method should be implemented");
		
	}

	@Override
	public Long countAllEntities() throws DAOException {
		
		throw new DAOException ("Method should be implemented");
	}

	@Override
	public List<Comment> readEntityListPagination(PaginationCriteria pageCrit) throws DAOException {
		
		throw new DAOException ("Method should be implemented");
	}
}
