package com.epam.newsmanagement.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.exception.UtilityException;

/**
 * Contains static utility methods for operations with Dates
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
public class DateUtility {

	private final static Logger logger = Logger.getRootLogger();

	private DateUtility() {

	}

	/**
	 * Creates a java.util.Date object from String representation of date
	 * 
	 * @param str
	 *            - String representation of date
	 * @return date
	 */
	public static Date dateFromString(String str) throws UtilityException {

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;

		try {

			date = formatter.parse(str);

		} catch (ParseException e) {
			
			logger.error("From String to java.util.Date convert exception", e);
			throw new UtilityException("From String to java.util.Date convert exception", e);
		}

		return date;
	}

}
