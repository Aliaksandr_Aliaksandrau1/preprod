package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utility.PaginationCriteria;

/**
 * Provides service operations for authors
 * 
 */
public interface AuthorService {

	/**
	 * Gets all authors from the DB
	 * 
	 * @return list of authors
	 * @throws ServiceException
	 */
	public List<Author> readAllAuthors() throws ServiceException;

	/**
	 * Returns the author by the identifier
	 * 
	 * @param id
	 *            the identifier of author
	 * @return author
	 * @throws ServiceException
	 */
	public Author readAuthorById(Long id) throws ServiceException;

	/**
	 * Creates a new Author
	 * 
	 * @param author
	 *            author which must be inserted
	 * @return the identifier of inserted author
	 * @throws ServiceException
	 */
	public Long createAuthor(Author author) throws ServiceException;

	/**
	 * Updates data about Author
	 * 
	 * @param author
	 *            the author to update
	 * @return true if the author was updated
	 * @throws ServiceException
	 */
	public boolean updateAuthor(Author author) throws ServiceException;

	/**
	 * Deletes all data about the author
	 * 
	 * @param id
	 *            the identifier of author
	 * @return true if author was deleted
	 * @throws ServiceException
	 */
	public boolean deleteAuthor(Long id) throws ServiceException;

	/**
	 * Receives the author selected by the news identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @return author the author relating the news
	 * 
	 * @throws ServiceException
	 */
	public Author readAuthorByNewsId(Long newsId) throws ServiceException;

	/**
	 * 
	 * Deletes Authors from DB by their ID
	 * 
	 * @param idList
	 *            - list of authors ID
	 * @return true if authors were deleted
	 * @throws ServiceException
	 */
	public boolean deleteListOfAuthors(List<Long> idList) throws ServiceException;
	
	/**
	 * Counts all authors
	 * 
	 * @return the number of all authors
	 * @throws ServiceException
	 */
	public Long countAllAuthors() throws ServiceException;
	
	
	/**
	 * Returns list of Authors according to pagination criteria
	 * 
	 * @param pageCrit
	 *            - pagination criteria
	 * @return list of authors
	 * @throws DAOException
	 */
	public List<Author> readEntityListPagination(PaginationCriteria pageCrit) throws ServiceException;

}
