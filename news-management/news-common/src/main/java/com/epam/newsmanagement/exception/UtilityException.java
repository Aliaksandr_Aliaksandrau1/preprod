package com.epam.newsmanagement.exception;

/**
 * Exception for utility classes and methods
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
public class UtilityException extends NewsManagementProjectException {

	private static final long serialVersionUID = -7574019743097307377L;

	public UtilityException() {

	}

	public UtilityException(String message) {
		super(message);
	}

	public UtilityException(Throwable cause) {
		super(cause);
	}

	public UtilityException(String message, Throwable cause) {
		super(message, cause);
	}
}
