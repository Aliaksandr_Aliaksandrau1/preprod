package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utility.ExceptionMessages;
import com.epam.newsmanagement.utility.PaginationCriteria;
import com.epam.newsmanagement.utility.SearchCriteria;

@Service
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	
	@Override
	public News readNewsById(Long id) throws ServiceException {
		try {

			return newsDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createNews(News news) throws ServiceException {
		try {
			
			return newsDAO.create(news);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateNews(News news) throws ServiceException {
		try {
			
			return newsDAO.update(news);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteNews(Long id) throws ServiceException {
		try {
			
			return newsDAO.delete(id);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			
			return newsDAO.setNewsAuthor(newsId, authorId);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countAllNews() throws ServiceException {
		try {
			
			return newsDAO.countAllEntities();
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<News> readAllNews() throws ServiceException {
		try {
			
			return newsDAO.readAllNews();
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			
			return newsDAO.readNewsBySearchCriteria(searchCriteria);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			
			return newsDAO.countNewsBySearchCriteria(searchCriteria);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}
		
	@Override
	public List<News> readEntityListPagination (PaginationCriteria pageCrit) throws ServiceException {
		try {
			
			return newsDAO.readEntityListPagination(pageCrit);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteListOfNews(List<Long> idList) throws ServiceException {
		try {

			return newsDAO.deleteListOfEntitiesById(idList);
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.NEWS_SERVICE_EXC + e, e);
		}
	}
	

}
