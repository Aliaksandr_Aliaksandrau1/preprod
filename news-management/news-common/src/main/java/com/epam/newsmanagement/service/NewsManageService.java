package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utility.PaginationCriteria;

public interface NewsManageService {
	
	//TODO add javadoc
	
	public Long createNews(News news) throws ServiceException;

	public Boolean updateNews(News news) throws ServiceException;

	public Boolean deleteNews(Long newsId) throws ServiceException;

	public List<News> readAllNews() throws ServiceException;

	public News readNewsById(Long newsId) throws ServiceException;
	

	
	public Long countAllNews() throws ServiceException;
	
	public List<News> readEntityListPagination(PaginationCriteria pageCrit) throws ServiceException;
	
	
	/*public Boolean setNewsAuthor(Long newsId, Long authorId) throws ServiceException;

	public List<NewsTO_old> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	

	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException;

	public List<Long> createComments(List<Comment_old> commentList) throws ServiceException;

	public Boolean deleteComments(List<Long> commentIdList) throws ServiceException;

	
*/


}
