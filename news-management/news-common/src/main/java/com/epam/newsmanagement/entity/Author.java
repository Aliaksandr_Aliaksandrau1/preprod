package com.epam.newsmanagement.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.epam.newsmanagement.utility.CustomJsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Representation of Author entity of the DB
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@Entity
@Table(name = "authors")
@SequenceGenerator(name = "default_gen", sequenceName = "AUTHORS_SEQ", allocationSize = 1)
@AttributeOverride(name = "id", column = @Column(name = "AUTHOR_ID"))
public class Author extends AbstractEntity {

	private static final long serialVersionUID = 9014379697751949712L;

	@Column(name = "AUTHOR_NAME", nullable = false, length = 30, columnDefinition = "nvarchar2")
	private String authorName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRED", nullable = true)
	Date expired;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors", cascade = CascadeType.ALL)
	private Set<News> news;

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	@JsonSerialize(using = CustomJsonDateSerializer.class)
	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorName=" + authorName + ", expired=" + expired + ", getId()=" + getId() + "]";
	}
}
