package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.AbstractEntity;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.PaginationCriteria;

/**
 * 
 * Represents common DAO operations on entity data from database
 * 
 * @param <T>
 *            - entity class
 */
public interface BaseDAO<T extends AbstractEntity> {

	/**
	 * Return Entity from the data source by the identifier of entity
	 * 
	 * @param id
	 *            the identifier of entity
	 * @return Entity
	 * @throws DAOException
	 */
	T readById(Long id) throws DAOException;

	/**
	 * Inserts new entity into the data source
	 * 
	 * @param entity
	 *            entity which must be inserted
	 * @return the identifier of inserted entity
	 * @throws DAOException
	 */
	Long create(T entity) throws DAOException;

	/**
	 * Deletes all data about Entity in the data source
	 * 
	 * @param id
	 *            the identifier of entity
	 * @return true if Entity was deleted
	 * @throws DAOException
	 */
	boolean delete(Long id) throws DAOException;

	/**
	 * Updates data about Entity in the data source
	 * 
	 * @param entity
	 *            the entity to update
	 * @return true if Entity was updated; false otherwise.
	 * @throws DAOException
	 */
	boolean update(T entity) throws DAOException;

	/**
	 * Deletes list of Entities from DB by ID
	 * 
	 * @param idList
	 * @return true if entities were deleted
	 * @throws DAOException
	 */
	public boolean deleteListOfEntitiesById(List<Long> idList) throws DAOException;

	/**
	 * Counts all entities in the data source
	 * 
	 * 
	 * @return the number of all entities in the data source
	 * @throws DAOException
	 */
	public Long countAllEntities() throws DAOException;

	/**
	 * Returns list of entities according to pagination criteria
	 * 
	 * @param pageCrit
	 *            - pagination criteria
	 * @return list of entities
	 * @throws DAOException
	 */
	public List<T> readEntityListPagination(PaginationCriteria pageCrit) throws DAOException;
}
