package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.PaginationCriteria;

@Transactional
public class TagDAOHiberImpl implements TagDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final String HQL_SELECT_ALL_TAGS = "FROM Tag";
	private static final String HQL_DELETE_TAG_BY_ID = "DELETE from Tag where id = :id";
	private static final String HQL_DELETE_LIST_OF_TAGS_BY_ID = "DELETE FROM Tag t WHERE t.id IN (:list)";

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readTags() throws DAOException {

		return sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_TAGS).list();

	}

	@Override
	public Tag readById(Long id) throws DAOException {

		return (Tag) sessionFactory.getCurrentSession().get(Tag.class, id);

	}

	@Override
	public Long create(Tag entity) throws DAOException {

		return (Long) sessionFactory.getCurrentSession().save(entity);

	}

	@Override
	public boolean delete(Long id) throws DAOException {

		boolean isDeleted = false;
		Query query = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_TAG_BY_ID);
		query.setParameter("id", id);

		if (query.executeUpdate() != 1) {
			throw new DAOException(String.format("the tag with id = %d was not deleted", id));
		} else {
			isDeleted = true;
		}

		return isDeleted;
	}

	@Override
	public boolean deleteListOfEntitiesById(List<Long> idList) throws DAOException {

		Query q = sessionFactory.getCurrentSession().createQuery(HQL_DELETE_LIST_OF_TAGS_BY_ID);
		q.setParameterList("list", idList);

		return (q.executeUpdate() >= 1);

	}

	@Override
	public boolean update(Tag entity) throws DAOException {

		Tag newTag = (Tag) sessionFactory.getCurrentSession().merge(entity);

		return entity.equals(newTag);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readTagsByNewsId(Long newsId) throws DAOException {

		try {

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Tag.class, "tag");
			criteria.createAlias("tag.news", "news");
			criteria.add(Restrictions.eq("news.id", newsId));

			return criteria.list();

		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> readEntityListPagination(PaginationCriteria pageCrit) throws DAOException {

		int maxItemsOnPage;
		int currentPage;

		if (pageCrit.getMaxItemsOnPage() != null && pageCrit.getMaxItemsOnPage() >= 1) {
			maxItemsOnPage = pageCrit.getMaxItemsOnPage();
		} else {
			maxItemsOnPage = 10; // TODO should read from property file
		}

		if (pageCrit.getCurrentPage() != null && pageCrit.getCurrentPage() >= 1) {
			currentPage = pageCrit.getCurrentPage();
		} else {
			currentPage = 1;
		}

		Query query = sessionFactory.getCurrentSession().createQuery(HQL_SELECT_ALL_TAGS);
		query.setFirstResult((currentPage - 1) * maxItemsOnPage);
		query.setMaxResults(maxItemsOnPage);

		return query.list();
	}

	@Override
	public Long countAllEntities() throws DAOException {
		try {

			return (Long) sessionFactory.getCurrentSession().createCriteria(Tag.class)
					.setProjection(Projections.rowCount()).uniqueResult();

		} catch (HibernateException he) {
			throw new DAOException(he);
		}
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws DAOException {

		throw new DAOException("Operation is not supported");

	}

}
