package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utility.ExceptionMessages;
import com.epam.newsmanagement.utility.PaginationCriteria;

@Service
public class TagServiceImpl implements TagService {

	private TagDAO tagDAO;

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	@Override
	public Tag readTagById(Long id) throws ServiceException {
		try {

			return tagDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createTag(Tag tag) throws ServiceException {
		try {

			return tagDAO.create(tag);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateTag(Tag tag) throws ServiceException {
		try {

			return tagDAO.update(tag);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteTag(Long id) throws ServiceException {
		try {

			return tagDAO.delete(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Tag> readTagsByNewsId(Long newsId) throws ServiceException {
		try {

			return tagDAO.readTagsByNewsId(newsId);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Boolean attachTagsToNews(Long newsId, List<Long> tagIdList) throws ServiceException {
		try {

			return tagDAO.attachTagsToNews(newsId, tagIdList);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Tag> readTags() throws ServiceException {
		try {

			return tagDAO.readTags();

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteListOfTags(List<Long> idList) throws ServiceException {
		try {

			return tagDAO.deleteListOfEntitiesById(idList);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Tag> readEntityListPagination(PaginationCriteria pageCrit) throws ServiceException {
		try {

			return tagDAO.readEntityListPagination(pageCrit);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long countAllTags() throws ServiceException {
		try {
			
			return tagDAO.countAllEntities();
			
		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.TAG_SERVICE_EXC + e, e);
		}
	}
}
