package com.epam.newsmanagement.exception;

/**
 * Exception for DAO layer
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
public class DAOException extends NewsManagementProjectException {

	private static final long serialVersionUID = -2689134211390170299L;

	public DAOException() {

	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
}
