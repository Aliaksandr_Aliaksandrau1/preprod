package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.utility.ExceptionMessages;

public class CommentServiceImpl implements CommentService {

	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	@Override
	public List<Comment> readAllComments() throws ServiceException {

		try {

			return commentDAO.readAll();

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}

	}

	@Override
	public Comment readCommentById(Long id) throws ServiceException {

		try {

			return commentDAO.readById(id);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean deleteComment(Long id) throws ServiceException {

		try {
			return commentDAO.delete(id);

		} catch (DAOException e) {

			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Long createComment(Comment comment) throws ServiceException {

		try {

			return commentDAO.create(comment);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public boolean updateComment(Comment comment) throws ServiceException {

		try {

			return commentDAO.update(comment);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public List<Comment> readCommentsByNewsId(Long newsId) throws ServiceException {

		try {

			return commentDAO.readCommentsByNewsId(newsId);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Boolean deleteCommentsById(List<Long> commentIdList) throws ServiceException {

		try {

			return commentDAO.deleteCommentsById(commentIdList);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

	@Override
	public Boolean deleteCommentsByNewsId(Long newsId) throws ServiceException {

		try {

			return commentDAO.deleteCommentsByNewsId(newsId);

		} catch (DAOException e) {
			throw new ServiceException(ExceptionMessages.COMMENT_SERVICE_EXC + e, e);
		}
	}

}
