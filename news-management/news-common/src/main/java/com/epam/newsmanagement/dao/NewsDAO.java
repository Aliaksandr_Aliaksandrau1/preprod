package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utility.SearchCriteria;

/**
 * Provides operations to process data for News from the data source
 *
 */
public interface NewsDAO extends BaseDAO<News> {
	
	/**
	 * Sets the author to the news by identifier
	 * 
	 * @param newsId
	 *            the news identifier
	 * @param authorId
	 *            the author identifier
	 * @return true if author was attached, false otherwise
	 * @throws DAOException
	 */
	public boolean setNewsAuthor(Long newsId, Long authorId) throws DAOException;

	

	
	// TODO add javadoc 
	
	public News readById(Long id) throws DAOException;

	

	public List<News> readAllNews() throws DAOException;
	public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;
	public Long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;

	
	

}
