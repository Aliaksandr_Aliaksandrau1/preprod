package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Mock
	private UserDAO userDAO;
	private UserServiceImpl userService;
	private User user = mock(User.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		userService = new UserServiceImpl();
		userService.setUserDAO(userDAO);
		user.setId(1L);
		user.setUserName("test name");
		user.setLogin("login1");
		user.setLogin("pass1");
		
		// TODO insert other data about user if necessary
	}

	@Test
	public void testReadUserById() throws Exception {
		userService.readUserById(user.getId());
		verify(userDAO).readById(user.getId());

	}
	@Test
	public void testReadUserByLoginAndPassword() throws Exception {
		userService.readUserByLoginAndPassword(user.getLogin(), user.getPassword());
		verify(userDAO).readUserByLoginAndPassword(user.getLogin(), user.getPassword());

	}

	
}
