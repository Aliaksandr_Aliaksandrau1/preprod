package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/dbunit_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

@DatabaseSetup(value = { "/UserDAOTest_data.xml" }, type = DatabaseOperation.CLEAN_INSERT)
public class UserDAOTest {

	@Autowired
	private UserDAO userDAO;

	@Test
	public void testReadById() throws Exception {
		Long id = new Long(1);
		List<String> roleList = new ArrayList<String>();
		roleList.add("user");
		roleList.add("admin");
		User userExp = userBuilder(id, "John Lennon", "login1", "pass1", roleList);

		User userAct = userDAO.readById(id);
		assertUser(userExp, userAct);
	}

	@Test
	public void testReadUserByLoginAndPassword() throws Exception {
		Long id = new Long(1);
		String login = "login1";
		String password = "pass1";
		List<String> roleList = new ArrayList<String>();
		roleList.add("user");
		roleList.add("admin");
		User userExp = userBuilder(id, "John Lennon", login, password, roleList);
		User userAct = userDAO.readUserByLoginAndPassword(login, password);
		assertUser(userExp, userAct);
	}

	private User userBuilder(Long userId, String userName, String login, String password, List<String> roleList) {
		User user = new User();
		user.setId(userId);
		user.setUserName(userName);
		user.setLogin(login);
		user.setPassword(password);
		user.setRoleList(roleList);
		return user;

	}

	private void assertUser(User userExp, User userAct) {
		Assert.assertNotNull(userAct);
		Assert.assertEquals(userExp.getId(), userAct.getId());
		Assert.assertEquals(userExp.getUserName(), userAct.getUserName());
		Assert.assertEquals(userExp.getLogin(), userAct.getLogin());
		Assert.assertEquals(userExp.getRoleList(), userAct.getRoleList());
		Assert.assertEquals(userExp, userAct);

	}

}
