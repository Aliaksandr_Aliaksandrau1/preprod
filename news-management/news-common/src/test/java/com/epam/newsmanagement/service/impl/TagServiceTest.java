package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private TagDAO tagDAO;
	private TagServiceImpl tagService;
	private Tag tag = mock(Tag.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		tagService = new TagServiceImpl();
		tagService.setTagDAO(tagDAO);
		tag.setId(1L);
		tag.setTagName("test name");
	}

	@Test
	public void readTagByIdTest() throws Exception {
		tagService.readTagById(tag.getId());
		verify(tagDAO).readById(tag.getId());

	}

	@Test
	public void createTagTest() throws Exception {
		tagService.createTag(tag);
		verify(tagDAO).create(tag);

	}

	@Test
	public void updateTagTest() throws Exception {
		tagService.updateTag(tag);
		verify(tagDAO).update(tag);

	}

	@Test
	public void deleteTagTest() throws Exception {
		tagService.deleteTag(tag.getId());
		verify(tagDAO).delete(tag.getId());

	}

	@Test
	public void readTagsByNewsIdTest() throws Exception {
		tagService.readTagsByNewsId(1L);
		verify(tagDAO).readTagsByNewsId(1L);

	}
	/*
	 * @Test public void readAllTagsTest() throws Exception {
	 * tagService.readAllTags(); verify(tagDAO).readAll();
	 * 
	 * }
	 */

}
