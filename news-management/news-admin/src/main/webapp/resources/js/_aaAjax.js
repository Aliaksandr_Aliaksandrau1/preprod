/**
 * aaAjax Module provides connection and data exchange with server by AJAX
 * technology in JSON format
 * 
 */

(function() {
	function exports(value) {

	}

	exports.handleAjaxReqResp = handleAjaxReqResp;
	exports.getDataAsModel = getDataAsModel;
	exports.createAjaxRequestObject = createAjaxRequestObject;

	window.aaAJAX = exports;

	function handleAjaxReqResp(ajaxReqObj) {
		console.log(ajaxReqObj); // CONSOLE

		var reqDataToJSON = ajaxReqObj.reqDataToJSON, httpMethod = ajaxReqObj.httpMethod, url = '/news-admin/'
				+ ajaxReqObj.getUrl(), xhr = new XMLHttpRequest(), respObj = {}, respDataFromServer = null, callback = ajaxReqObj.callback;

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				
				respDataFromServer = JSON.parse(xhr.responseText);
				var model = new Model(respDataFromServer);

				if (ajaxReqObj.getDataForResponse()) {
					model.setConfig(ajaxReqObj.getDataForResponse());
				}
				
				console.log(model); // CONSOLE LOG
				ajaxReqObj.callback(model);
			}
		};

		xhr.open(httpMethod, url, true);
		xhr.setRequestHeader('Content-Type', 'application/json')
		xhr.send(JSON.stringify(reqDataToJSON));
	}
	
	/** Creates a request object for AJAX handler */
	function AjaxRequestObject(url, callback, reqDataToJSON, httpMethod) {

		if (!(this instanceof AjaxRequestObject)) {
			return new AjaxRequestObject(url, callback, reqDataToJSON,
					httpMethod);
		}

		this.url = url;
		this.callback = callback;
		this.reqDataToJSON = reqDataToJSON;

		/**
		 * set HTTP method for AJAX request default value is 'GET'
		 */
		if (httpMethod === 'GET' || httpMethod === 'POST') {
			this.httpMethod = httpMethod;
		}

	}

	function getDataAsModel(dataObject) {
		return new Model(dataObject);
	}

	function createAjaxRequestObject(url, callback, reqDataToJSON, httpMethod) {
		return new AjaxRequestObject(url, callback, reqDataToJSON, httpMethod);
	}

	/**
	 * Returns callback function
	 * 
	 * @returns
	 */
	
	AjaxRequestObject.prototype.getCallback = function() {
		return this.callback;
	};

	AjaxRequestObject.prototype.getUrl = function() {
		return this.url;
	};

	AjaxRequestObject.prototype.getRequestDataToJSON = function() {
		return this.reqDataToJSON;
	};

	AjaxRequestObject.prototype.setDataForResponse = function(data) {
		this.dataForResponse = data;
	};

	AjaxRequestObject.prototype.getDataForResponse = function() {
		return this.dataForResponse;
	};

	AjaxRequestObject.prototype.httpMethod = 'GET';

}());
