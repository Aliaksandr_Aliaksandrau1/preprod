/**
 * The module was designed for News-Management application. It contains
 * functions for interaction with News-Management app back-end.
 * 
 */
;
(function() {

	function exports(value) {
		// ...
	}

	var ajaxHandler = aaAJAX;

	exports.showNewsGridPagination = showNewsGridPagination;
	exports.showTagGridPagination = showTagGridPagination;
	exports.showAuthorGridPagination = showAuthorGridPagination;

	exports.updateTagAction = updateTagAction;
	exports.updateNewsAction = updateNewsAction;
	exports.updateAuthorAction = updateAuthorAction;

	// sets the exports function to global variable NM
	window.NM = exports;

	createCommonElements();

	function showNewsGridPagination(page, maxItemsOnPage) {

		if (!page) {
			page = 1;
		}
		if (!maxItemsOnPage) {
			maxItemsOnPage = 5
		}

		var paginationCriteria = {
			currentPage : page,
			maxItemsOnPage : maxItemsOnPage
		};

		document.getElementById('gridDiv').innerHTML = "";

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject(
				'viewNewsListPagination', function(param) {
					gridMaker(param);
				}, paginationCriteria, 'POST');

		var gridConfig = {
			columnNameArr : [ '', 'Title', 'Short text', 'Creation Date' ],
			dataNameArr : [ "id", "title", "shortText", "creationDate" ],
			entityUpdateFunction : NM.updateNewsAction

		};

		ajaxReqObj.setDataForResponse(gridConfig);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		handleControlPanelListener('news');

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('countAllNews',
				function(param) {
					createPaginationPanel(param, 'NM.showNewsGridPagination');
				}, null);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	function showTagGridPagination(page, maxItemsOnPage) {

		if (!page) {
			page = 1;
		}
		if (!maxItemsOnPage) {
			maxItemsOnPage = 5
		}

		var paginationCriteria = {
			currentPage : page,
			maxItemsOnPage : maxItemsOnPage
		};

		document.getElementById('gridDiv').innerHTML = "";

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject(
				'viewTagListPagination', function(param) {
					gridMaker(param);
				}, paginationCriteria, 'POST');

		var gridConfig = {
			columnNameArr : [ '', 'Tag Name' ],
			dataNameArr : [ 'id', 'tagName' ],
			entityUpdateFunction : NM.updateTagAction
		};

		ajaxReqObj.setDataForResponse(gridConfig);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		handleControlPanelListener('tag');

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('countAllTags',
				function(param) {
					createPaginationPanel(param, 'NM.showTagGridPagination');
				}, null);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	function showAuthorGridPagination(page, maxItemsOnPage) {

		var paginationCriteria = {
			currentPage : page,
			maxItemsOnPage : maxItemsOnPage
		};

		document.getElementById('gridDiv').innerHTML = "";

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject(
				'viewAuthorListPagination', function(param) {
					gridMaker(param);
				}, paginationCriteria, 'POST');

		var gridConfig = {
			columnNameArr : [ '', 'Author Name', 'Expired Date' ],
			dataNameArr : [ 'id', 'authorName', 'expired' ],
			entityUpdateFunction : NM.updateAuthorAction

		};

		ajaxReqObj.setDataForResponse(gridConfig);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		handleControlPanelListener('author');

		var ajaxReqObj = ajaxHandler
				.createAjaxRequestObject('countAllAuthors',
						function(param) {
							createPaginationPanel(param,
									'NM.showAuthorGridPagination');
						}, null);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	/* DELETE ENTITY ACTION FUNCTIONS */

	function deleteNewsAction() {
		var idListToDelete = getCheckedCheckboxesFor('entityChbx');
		ajaxProcessingCommonAction('deleteNewsList', showNewsGridPagination,
				idListToDelete, 'POST');
	}

	function deleteTagAction() {
		var idListToDelete = getCheckedCheckboxesFor('entityChbx');
		ajaxProcessingCommonAction('deleteTagList', showTagGridPagination,
				idListToDelete, 'POST');
	}

	function deleteAuthorAction() {
		var idListToDelete = getCheckedCheckboxesFor('entityChbx');
		ajaxProcessingCommonAction('deleteAuthorList',
				showAuthorGridPagination, idListToDelete, 'POST');
	}

	function ajaxProcessingCommonAction(url, callbackAfterAction, dataToJSON,
			httpMethod) {

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject(url, function() {
			callbackAfterAction();
		}, dataToJSON, httpMethod);
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);
	}

	/* ADD ENTITY ACTION FUNCTIONS */

	function addTagAction() {
		var formData = getFormDataAsObject('addForm');
		ajaxProcessingCommonAction('addTag', showTagGridPagination, formData,
				'POST');

	}

	function addAuthorAction() {

		var formData = getFormDataAsObject('addForm');
		ajaxProcessingCommonAction('addAuthor', showAuthorGridPagination,
				formData, 'POST');

	}

	function addNewsAction() {
		
		var formData = getFormDataAsObject('addForm');
		formData.tag = getSelectedOptionsAsArrayOfObjectsFor('selectTag');
		formData.author = getSelectedOptionsAsArrayOfObjectsFor('selectAuthor');

		ajaxProcessingCommonAction('addNews', showNewsGridPagination,
				formData, 'POST');
	}

	/* UPDATE ENTITY ACTION FUNCTIONS */

	function updateTagAction(entityObjectToUpdate) {
		var formData = null;

		console.log(entityObjectToUpdate);

		if (entityObjectToUpdate == null) {
			formData = getFormDataAsObject('updateForm');

		} else {
			formData = entityObjectToUpdate;
		}

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('updateTag',
				function() {
					showTagGridPagination();
				}, formData, 'POST');
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	function updateAuthorAction(entityToUpdate) {

		var formData = null;
		entityToUpdate ? formData = entityToUpdate
				: formData = getFormDataAsObject('updateForm');

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('updateAuthor',
				function() {
					showAuthorGridPagination();
				}, formData, 'POST');
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	function updateNewsAction(entityObjectToUpdate) {
		var formData = null;

		if (!entityObjectToUpdate) {
			formData = getFormDataAsObject('updateForm');

		} else {
			formData = entityObjectToUpdate;
		}

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('updateNews',
				function() {
					showNewsGridPagination();
				}, formData, 'POST');
		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

	}

	/**
	 * PRIVATE FUNCTIONS
	 */

	function gridMaker(param) {

		var grid = GRID.createGrid(param);
		window.grid = grid;
		console.log(grid);
		document.getElementById('gridDiv').innerHTML = grid.getGridAsTableStr();

		setEventOnTableCell();

	}

	function createCommonElements() {
		var contentElementDOM = document.getElementById('content');
		controlPanelMaker();

	}

	function createPaginationPanel(model, pagingFunction) {

		var data = model.getDataObject();
		var maxAmountOnPage = 5;
		var entityQuantity = data;
		var pagingPanelStr = "";
		var pagPanel = document.getElementById('paginationPanel');
		pagPanel.innerHTML = "";

		if (maxAmountOnPage < entityQuantity) {

			var pageQuantity = ((entityQuantity % maxAmountOnPage) === 0) ? entityQuantity
					/ maxAmountOnPage
					: entityQuantity / maxAmountOnPage + 1;
			for (var i = 1; i <= pageQuantity; i++) {

				pagingPanelStr += "<button onclick='" + pagingFunction + "("
						+ i + "," + maxAmountOnPage
						+ ");return false;' class='control-button'  value='"
						+ i + "'>" + i + " </button>";

			}

			pagPanel.innerHTML = pagingPanelStr;

		}

	}

	/**
	 * Creates DOM element ControlPanel with Add/Update/Delete buttons
	 */
	function controlPanelMaker() {

		var btnDeleteStr = "<button id='btnDel' class='control-button' value = 'Delete'>Delete</button>";
		var btnAddStr = "<button id='btnAdd' class='control-button'value = 'Add'>Add</button>";
		var btnUpdateStr = "<button id='btnUpd' class='control-button' value = 'Update'>Update</button>";
		var actionSuccessMessageStr = "<p id='actMessage'> </p>";
		var controlPanel = document.getElementById('controlPanel');

		controlPanel.innerHTML = btnAddStr + btnUpdateStr + btnDeleteStr
				+ actionSuccessMessageStr;

	}

	function buildModalWindowToDeleteNews() {
		buildDeleteModalWindow(deleteNewsAction,
				'Do you realy want to delete News?');
	}

	function buildModalWindowToDeleteTag() {
		buildDeleteModalWindow(deleteTagAction,
				'Do you realy want to delete Tags?');
	}

	function buildModalWindowToDeleteAuthor() {
		buildDeleteModalWindow(deleteAuthorAction,
				'Do you realy want to delete Authors?');
	}

	function buildTagAddModalWindow() {

		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent');
		var modalForm = document.createElement('div');
		var addFormStartStr = "<form id='addForm' name = 'addForm'>"
		var addFormEndStr = "</form>";
		var inputTagNameStr = "<input type='text' id='tagName' name='tagName'> </input>";
		var labelTagNameStr = "<label for='tagName'> Enter tag name: </label>";

		modalForm.innerHTML = addFormStartStr + inputTagNameStr
				+ labelTagNameStr + addFormEndStr;

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', addTagAction, false);
		modal.insertBefore(modalForm, btnSubmit);
		
	/*	document.getElementById('tagName').addEventListener('blur', function() {inlineInputValidation('tagName');
		})*/

	}

	function buildAuthorAddModalWindow() {

		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent');
		var modalForm = document.createElement('div');
		var addFormStartStr = "<form id='addForm' name = 'addForm'>"
		var addFormEndStr = "</form>";
		var inputAuthorNameStr = "<input type='text' id='authorName' name='authorName'> </input>";
		var labelAuthorNameStr = "<label for='authorName'> Enter author name: </label>";
		var inputAuthorExpiredStr = "<input type = 'date' id = 'expired' name = 'expired'> </input>";
		var labelAuthorExpiredStr = "<label for='expired'> Enter expired date: </label>";

		modalForm.innerHTML = addFormStartStr + labelAuthorNameStr
				+ inputAuthorNameStr + labelAuthorExpiredStr
				+ inputAuthorExpiredStr + addFormEndStr;

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', addAuthorAction, false);
		modal.insertBefore(modalForm, btnSubmit);

	}

	function buildNewsAddModalWindow() {

		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent');
		var modalForm = document.createElement('div');
		var currentDate = getCurrentDate();
		var addFormStartStr = "<form id='addForm' name = 'addForm'>"
		var addFormEndStr = "</form>";
		var inputTitleStr = "<input type='text' id='title' name='title'> </input>";
		var labelTitleStr = "<label for='title'> Enter title: </label>";
		var inputShortTextStr = "<input type='text' id='shortText' name='shortText'> </input>";
		var labelShortTextStr = "<label for='shortText'> Enter short text: </label>";
		var inputFullTextStr = "<input type='text' id='fullText' name='fullText'> </input>";
		var labelFullTextStr = "<label for='fullText'> Enter full text: </label>";
		var inputCreationDateStr = "<input type = 'hidden' style='display: none' id = 'creationDate' name = 'creationDate' value = '"
				+ currentDate + "'> </input>";
		var inputModificationDateStr = "<input type = 'hidden' style='display: none'  id = 'modificationDate' name = 'modificationDate' value = '"
				+ currentDate + "'> </input>";

		modalForm.innerHTML = addFormStartStr + labelTitleStr + inputTitleStr
				+ labelShortTextStr + inputShortTextStr + labelFullTextStr
				+ inputFullTextStr + inputCreationDateStr
				+ inputModificationDateStr + addFormEndStr;

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', addNewsAction, false);
		modal.insertBefore(modalForm, btnSubmit);

		var ajaxReqObjectTag = ajaxHandler.createAjaxRequestObject(
				'viewTagList', function(model) {
					createTagSelect(model);
				}, null, 'GET');

		var tagList = ajaxHandler.handleAjaxReqResp(ajaxReqObjectTag);

		ajaxReqObjectTag = ajaxHandler.createAjaxRequestObject(
				'viewAuthorList', function(model) {
					createAuthorSelect(model);
				}, null, 'GET');

		var tagList = ajaxHandler.handleAjaxReqResp(ajaxReqObjectTag);

		function createTagSelect(model) {

			var selectTagDiv = document.createElement('div');

			selectTagDiv.id = "selectTagDiv";
			selectTagDiv.setAttribute('class', 'mainselection');
			// selectTagDiv.setAttribute('class', 'modal-content');

			var selectStartStr = "<select multiple id = 'selectTag' name = 'selectTag'> ";
			var selectEndStr = "</select>";
			var data = model.getDataObject();
			var selectOptionStr = "";

			for (var i = 0; i < data.length; i++) {

				selectOptionStr += "<option value='" + data[i].id + "'>"
						+ data[i].tagName + "</option>";

			}

			selectTagDiv.innerHTML = selectStartStr + selectOptionStr
					+ selectEndStr;

			modal.insertBefore(selectTagDiv, btnSubmit);

		}

		function createAuthorSelect(model) {

			var selectAuthorDiv = document.createElement('div');
			selectAuthorDiv.id = "selectAuthorDiv";
			selectAuthorDiv.setAttribute('class', 'mainselection');
			var selectStartStr = "<select id = 'selectAuthor' name = 'selectAuthor'> ";
			var selectEndStr = "</select>";
			var data = model.getDataObject();
			var selectOptionStr = "";

			for (var i = 0; i < data.length; i++) {

				selectOptionStr += "<option value='" + data[i].id + "'>"
						+ data[i].authorName + "</option>";

			}

			selectAuthorDiv.innerHTML = selectStartStr + selectOptionStr
					+ selectEndStr;
			modal.insertBefore(selectAuthorDiv, btnSubmit);

		}

	}

	function buildAuthorUpdateModalWindow() {
		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent');
		var authorToUpdate = (getCheckedCheckboxesFor('entityChbx'))[0];

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('readAuthorById',
				function(model) {
					createUpdateAuthorForm(model);
				}, authorToUpdate, 'POST');

		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', function() {
			updateAuthorAction();
		}, false);

		function createUpdateAuthorForm(model) {

			var data = model.getDataObject();
			var modalForm = document.createElement('div');
			var updateFormStartStr = "<form id='updateForm' name = 'updateForm'>"
			var updateFormEndStr = "</form>";
			var inputIdStr = "<input type='hidden' id='id' name='id' value='"
					+ data.id + "'> </input>";
			var inputAuthorNameStr = "<input type='text' id='authorName' name='authorName' value = '"
					+ data.authorName + "'> </input>";
			var labelAuthorNameStr = "<label for='authorName'> Enter author name: </label>";

			var inputExpiredStr = "<input type='date' id='expired' name='expired' value = '"

					+ data.expired + "'> </input>";
			var labelExpiredStr = "<label for='expired'> Enter expired date: </label>";

			modalForm.innerHTML = updateFormStartStr + inputIdStr
					+ labelAuthorNameStr + inputAuthorNameStr + labelExpiredStr
					+ inputExpiredStr + updateFormEndStr;
			modal.insertBefore(modalForm, btnSubmit);
		}
	}

	function buildTagUpdateModalWindow() {
		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent');
		var tagToUpdate = (getCheckedCheckboxesFor('entityChbx'))[0];

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('readTagById',
				function(model) {
					createUpdateTagForm(model);
				}, tagToUpdate, 'POST');

		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', function (){updateTagAction();}, false);

		function createUpdateTagForm(model) {

			var data = model.getDataObject();
			var modalForm = document.createElement('div');
			var updateFormStartStr = "<form id='updateForm' name = 'updateForm'>"
			var updateFormEndStr = "</form>";
			var inputIdStr = "<input type='hidden' id='id' name='id' value='"
					+ data.id + "'> </input>";
			var inputTagNameStr = "<input type='text' id='tagName' name='tagName' value = '"
					+ data.tagName + "'> </input>";
			var labelTagNameStr = "<label for='tagName'> Enter tag name: </label>";

			modalForm.innerHTML = updateFormStartStr + inputIdStr
					+ labelTagNameStr + inputTagNameStr + updateFormEndStr;
			modal.insertBefore(modalForm, btnSubmit);
		}

	}

	function buildNewsUpdateModalWindow() {

		commonModalWindowBuilder();

		var currentDate = getCurrentDate();
		var modal = document.getElementById('modalContent');
		var newsToUpdate = (getCheckedCheckboxesFor('entityChbx'))[0];

		var ajaxReqObj = ajaxHandler.createAjaxRequestObject('readNewsById',
				function(model) {
					createUpdateNewsForm(model);
				}, newsToUpdate, 'POST');

		ajaxHandler.handleAjaxReqResp(ajaxReqObj);

		var btnSubmit = document.getElementById('btnSubmitModal');
		btnSubmit.addEventListener('click', function(){updateNewsAction();}, false);

		function createUpdateNewsForm(model) {

			var data = model.getDataObject();
			var modalForm = document.createElement('div');
			var updateFormStartStr = "<form id='updateForm' name = 'updateForm'>"
			var updateFormEndStr = "</form>";
			var inputIdStr = "<input type='hidden' id='id' name='id' value='"
					+ data.id + "'> </input>";

			var inputTitleStr = "<input type='text' id='title' name='title' value = '"
					+ data.title + "'> </input>";
			var labelTitleStr = "<label for='title'> Enter title: </label>";

			var inputShortTextStr = "<input type='text' id='shortText' name='shortText' value = '"
					+ data.shortText + "'> </input>";
			var labelShortTextStr = "<label for='shortText'> Enter short text: </label>";

			var inputFullTextStr = "<input type='text' id='fullText' name='fullText' value = '"
					+ data.fullText + "'> </input>";
			var labelFullTextStr = "<label for='fullText'> Enter full text: </label>";

			var inputCreationDateStr = "<input type = 'hidden' id = 'creationDate' name = 'creationDate' value = '"

					+ data.creationDate + "'> </input>";

			var inputModificationDateStr = "<input type = 'hidden' id = 'modificationDate' name = 'modificationDate' value = '"
					+ currentDate + "'> </input>";

			modalForm.innerHTML = updateFormStartStr + inputIdStr
					+ labelTitleStr + inputTitleStr + labelShortTextStr
					+ inputShortTextStr + labelFullTextStr + inputFullTextStr
					+ inputCreationDateStr + inputModificationDateStr
					+ updateFormEndStr;

			modal.insertBefore(modalForm, btnSubmit);
		}

	}

	function buildDeleteModalWindow(deleteFunction, confirmMessage) {

		var deleteFunction_ = deleteFunction;
		var confirmMessage_ = confirmMessage;

		commonModalWindowBuilder();

		var modal = document.getElementById('modalContent'), message = document
				.createElement('p'), btnSubmit = document
				.getElementById('btnSubmitModal');

		btnSubmit.addEventListener('click', deleteFunction_, false);

		message.innerHTML = confirmMessage_;
		modal.insertBefore(message, btnSubmit);

	}

	/**
	 * Builds modal window with common elements
	 */
	function commonModalWindowBuilder() {

		var modal = document.getElementById('myModal');
		var btnSubmitStr = "<button id='btnSubmitModal' class='control-button' value = 'Submit'>Submit</button>";
		var btnCancelStr = "<button id='btnCancelModal' class='control-button' value = 'Cancel'>Cancel</button>";

		while (modal.firstChild) {
			modal.removeChild(modal.firstChild);
		}

		modal.innerHTML = "<div class='modal-content' id = 'modalContent'>"
				+ btnSubmitStr + btnCancelStr + "</div>";

		document.getElementById('btnCancelModal').addEventListener('click',
				closeModalWindow, false);
		document.getElementById('btnSubmitModal').addEventListener('click',
				closeModalWindow, false);

		modal.style.display = "block";
	}

	function handleControlPanelListener(entityType) {

		var entityType_ = entityType;
		var btnDel = document.getElementById('btnDel');
		var btnDelClone = btnDel.cloneNode(true);

		var btnAdd = document.getElementById('btnAdd');
		var btnAddClone = btnAdd.cloneNode(true);

		var btnUpdate = document.getElementById('btnUpd');
		var btnUpdateClone = btnUpdate.cloneNode(true);

		var delModalWindowBuilder = null;
		var addModalWindowBuilder = null;
		var updateModalWindowBuilder = null;

		switch (entityType_) {
		case 'tag': {
			delModalWindowBuilder = buildModalWindowToDeleteTag;
			addModalWindowBuilder = buildTagAddModalWindow;
			updateModalWindowBuilder = buildTagUpdateModalWindow;
			break;
		}
		case 'news': {
			delModalWindowBuilder = buildModalWindowToDeleteNews;
			addModalWindowBuilder = buildNewsAddModalWindow;
			updateModalWindowBuilder = buildNewsUpdateModalWindow;
			break;
		}
		case 'author': {
			delModalWindowBuilder = buildModalWindowToDeleteAuthor;
			addModalWindowBuilder = buildAuthorAddModalWindow;
			updateModalWindowBuilder = buildAuthorUpdateModalWindow;
			break;
		}
		}

		btnDel.parentNode.replaceChild(btnDelClone, btnDel);
		btnUpdate.parentNode.replaceChild(btnUpdateClone, btnUpdate);
		btnAdd.parentNode.replaceChild(btnAddClone, btnAdd);

		btnDelClone.addEventListener('click', delModalWindowBuilder);
		btnAddClone.addEventListener('click', addModalWindowBuilder);
		btnUpdateClone.addEventListener('click', updateModalWindowBuilder);

	}

	/**
	 * Makes the modal window to be non-displayed
	 */
	function closeModalWindow() {
		var modal = document.getElementById('myModal');
		modal.style.display = "none";
	}

	// UTILITY FUNCTIONS

	function setEventOnTableCell() {

		var cell = null;
		var table = document.getElementById("dataTable");
			
		if (table != null) {
			for (var i = 1; i < table.rows.length; i++) {
				for (var j = 1; j < table.rows[i].cells.length; j++)

					table.rows[i].cells[j].ondblclick = function() {

						var cell = this;
						var value = cell.innerHTML;

						window.addEventListener('click', function(e) {
							documentEvent(e);
						});
						
						cell.innerHTML = "<input type='text' id='inpInlineFormat' value='"
								+ value + "'></input>";

						cell.addEventListener("keypress", function(e) {
							var key = e.which || e.keyCode;
							if (key === 13) { 

								var newValue = cell.firstChild.value;
								cell.removeChild(cell.firstChild);
								cell.innerHTML = newValue;
								
								
								var cellRowAndColumnArr = cell.id.split("-");
								var objectNumberInDataList = cellRowAndColumnArr[0];
							
								
								console.log(objectNumberInDataList);
								
								var dataObj = window.grid.getModel().getDataObject()[objectNumberInDataList];
								var updateFunction = window.grid.getModel().getConfig().entityUpdateFunction;
								console.log(updateFunction);
								
								
								var oldValue = dataObj[cell.getAttribute("name")];
								
								console.log(newValue);
			
								
								dataObj[cell.getAttribute("name")] = newValue;
								
								
								if(oldValue!== newValue) {
									updateFunction(dataObj);
								
								}
								
							}
						}, false);

					};

			}
		}

		function documentEvent(e) {

			console.log(e.target);

			if (!(e.target.id === 'inpInlineFormat')&& document.getElementById("inpInlineFormat")) {
				var cell = document.getElementById("inpInlineFormat").parentNode;
				var value = cell.firstChild.value;

				cell.removeChild(cell.firstChild);
				cell.innerHTML = value;
			}

		}
	}


	
	
	/*PaginationCriteria.prototype = {
			constructor: function () {
		console.log(1);
			},

			field1: {

			},

			field2: "sthsth",

			myMethod: function () {

			}
		}*/
	
	
	
	
	
	/* create array of checked checkboxes */
	function getCheckedCheckboxesFor(checkboxName) {
		var checkboxes = document.querySelectorAll('input[name="'
				+ checkboxName + '"]:checked'), values = [];
		Array.prototype.forEach.call(checkboxes, function(el) {
			values.push(el.value);
		});
		return values;
	}

	function getFormDataAsObject(formId) {
		var objForJSON = {};
		var elements = document.getElementById(formId).elements;
		for (var i = 0, element; element = elements[i++];) {

			if (element.type === "text" && element.value !== "") {
				objForJSON[element.name] = element.value;
			}
			else if (element.type === "hidden" && element.value !== "") {
				objForJSON[element.name] = element.value;
			} else if (element.type === "date" && element.value !== "") {
				objForJSON[element.name] = element.value;
			}

		}
		console.log(objForJSON); // CONSOLE LOG
		return objForJSON;
	}

	function getSelectedOptionsAsArrayOfObjectsFor(selectId) {

		var select = document.getElementById(selectId);
		var selOpt = select.selectedOptions;
		var arrData = [];
		var element = {};

		for (var i = 0; i < selOpt.length; i++) {
			element = {};
			element.id = selOpt[i].value;
			element.tagName = selOpt[i].innerHTML;
			arrData.push(element)
		}

		return arrData;
	}

	function getCurrentDate() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; // January is 0!
		var yyyy = today.getFullYear();

		if (dd < 10) {
			dd = '0' + dd
		}

		if (mm < 10) {
			mm = '0' + mm
		}

		today = yyyy + '-' + mm + '-' + dd;

		return today;
	}

}());
