/**
 * Class Model 
 * For holding data from server
 */

/**
 * Model constructor
 */
function Model(responseDataFromServer, config) {
	
	/**
	 * Data responded from server in JS Object format
	 */
	this.data_ = responseDataFromServer;
	
	/**
	 * Configuration object, that contains:
	 *   - array of column names for Grid
	 *   - array of data object field names that should be shown in Grid
	 *   - name of updateEntity function that will used in inline formatting 
	 *   
	 *   Configuration object should be created and added 
	 *   to the request object for AJAX before AJAX handling
	 */
	this.config_ = config;

}

/**
 * Receives value of field by field name value of field in data object
 * 
 * @param key
 * @returns field value
 */
Model.prototype.getValueByKey = function(key) {
	return this.data_[key];
};

/**
 * Returns array of field names of data object
 * 
 * @returns Array
 */
Model.prototype.getFieldNamesAsArray = function() {
	return Object.keys(this.data_);
};

/**
 * Return data from server as Object
 * 
 * @returns data object
 */
Model.prototype.getDataObject = function() {
	return this.data_;
};

Model.prototype.setConfig = function(config) {
	this.config_ = config;
};

Model.prototype.getConfig = function() {
	return this.config_;
};
