/**
 *  aaGrid module provides functions to create Grid and GridConfig onjects
 * 
 * 
 */

;
(function() {

	function exports(value) {
		// ...
	}

	// show news table
	exports.createGrid = createGrid;
	exports.createGridConfig = createGridConfig;

	// set the exports function to global variable GRID
	window.GRID = exports;

	function createGrid(model, config) {

		return new Grid(model, config);
	}

	function Grid(model, config) {

		this.gridDiv = document.createElement('div');

		var model_ = model;
		var gridConfig = model.getConfig();

		console.log(model_); // CONSOLE LOG
		console.log(config); // CONSOLE LOG

		var colNamesArr = gridConfig.columnNameArr;
		var dataFieldsNameArr = gridConfig.dataNameArr;
		var entityUpdateFunction = gridConfig.entityUpdateFunction;
		var dataArr = model_.getDataObject();
		var gridTableStr = "";
		gridTableStr += "<table id='dataTable'>  <tr> ";

		for (var i = 0; i < colNamesArr.length; i++) {
			gridTableStr += "<th>" + colNamesArr[i] + "</th>";
		}

		gridTableStr += "</tr>";

		for (var i = 0; i < dataArr.length; i++) {

			var object = dataArr[i];

			gridTableStr += "<tr id='tr_" + i + "'>";
			gridTableStr += "<td><input type='checkbox' name='entityChbx' onclick = 'function(){}'  value='"
					+ object[dataFieldsNameArr[0]] + "'></input></td>";

			for (var j = 1; j < colNamesArr.length; j++) {

				gridTableStr += "<td id = '" + i + "-" + j + "' name = '"
						+ dataFieldsNameArr[j] + "'>"
						+ dataArr[i][(dataFieldsNameArr[j])] + "</td>";
			}

			gridTableStr += "</tr>";

		}

		gridTableStr += "</table>";

		this.gridTableStr = gridTableStr;
		this.model_ = model_;

		gridDiv.innerHTML = gridTableStr;

	}

	/*
	 * Public functions
	 * 
	 */

	function createGridConfig(columnNamesArr, dataFieldNamesArr) {
		return new GridConfig(columnNamesArr, dataFieldNamesArr);
	}

	function GridConfig(columnNamesArr, dataFieldNamesArr) {
		this.columnNamesArr_ = columnNamesArr;
		this.dataFieldNamesArr_ = dataFieldNamesArr;
	}

	

	function getGridAsDivElement() {
		return this.gridDiv;
	}

	/*
	 * Utility functions
	 */

	Grid.prototype.getGridAsDivElementDOM = function() {
		return this.gridDiv;
	}

	Grid.prototype.getGridAsTableStr = function() {
		return this.gridTableStr;
	}

	Grid.prototype.getModel = function() {
		return this.model_;
	}

	GridConfig.prototype.getColumnNamesArray = function() {
		return this.columnNamesArr_;
	}

	GridConfig.prototype.getDataFieldNamesArray = function() {
		return this.dataFieldNamesArr_;
	}

}());