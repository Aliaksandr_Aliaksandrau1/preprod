<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page session="true"%>
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet"
	type="text/css">
<title>Login page</title>
</head>
<body>

	<c:if test="${not empty error}">
		<div class="error">${error}</div>
	</c:if>
	<c:if test="${not empty msg}">
		<div class="msg">${msg}</div>
	</c:if>

	<form class='login-form' name='login-form'
		action="<c:url value='/j_spring_security_check' />" method='POST'>

		Login: <input type="text" name="username" value="admin">
		Password: <input type="password" name="password" value="admin" /> <input
			name="submit" type="submit" class='button' value="Login" /> <input
			type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

</body>
</html>