<!DOCTYPE html>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@page session="true"%>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"
	type="text/css">
</head>

<body>
	<header>
		<h2>
			<s:message code="general.message.head" text="default text" />
		</h2>
	</header>

	<div class="sidebar">

		<a id="newslist" href="#"
			onclick="NM.showNewsGridPagination();return false;">News</a> <a
			id="authorListLink" href="#"
			onclick="NM.showAuthorGridPagination();return false;">Authors</a> <a
			id="tagListLink" href="#"
			onclick="NM.showTagGridPagination();return false;">Tags</a> <a
			href="<c:url value="/logout" />"> Logout</a>

	</div>
	<div class="content" id="content">

		<div id="myModal" class="modal"></div>
		<div id="controlPanel" class="control-panel"></div>
		<div id="gridDiv"></div>
		<div id="paginationPanel" class="control-panel"></div>

	</div>

	<footer>
		<p>Copyright @ EPAM 2016. All rights reserved</p>
	</footer>

	<script src="<c:url value="/resources/js/_aaModel.js"/>"></script>
	<script src="<c:url value="/resources/js/_aaAjax.js"/>"></script>
	<script src="<c:url value="/resources/js/_aaGrid.js"/>"></script>
	<script src="<c:url value="/resources/js/_aaNM.js"/>"></script>
	<script src="<c:url value="/resources/js/_start.js"/>"></script>

</body>
</html>
