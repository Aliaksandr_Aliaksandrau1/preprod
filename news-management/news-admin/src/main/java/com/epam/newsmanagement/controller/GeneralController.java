package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Provides redirection to the main page and LogIn/LogOut operations for app
 * users
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@Controller
public class GeneralController {

	/**
	 * Opens the main page of web app
	 * 
	 * @return the name of the main JSP page
	 */
	@RequestMapping("/admin")
	public String mainPage() {

		return "main";

	}

	/**
	 * Opens the Login Page
	 * 
	 * @return the login page name
	 */
	@RequestMapping("/loginPage")
	public String login() {

		return "loginPage";

	}

	/**
	 * 
	 * Redirects to the login page in case of unauthorized access attempts
	 * 
	 * @param HttpServletRequest
	 * @param HttpServletResponse
	 * @return redirect command and the login page name
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}

		return "redirect:/loginPage";
	}

}
