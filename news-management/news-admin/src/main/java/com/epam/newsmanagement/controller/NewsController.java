package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utility.PaginationCriteria;

/**
 * Provide operations with News entity
 * 
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@RestController
public class NewsController {

	@Autowired
	private NewsService newsService;

	public void setNewsService(NewsService newsService) {

		this.newsService = newsService;
	}

	/**
	 * Gets the list of all news from DB
	 * 
	 * @return list of all News
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewNewsList", method = RequestMethod.GET)
	public List<News> readNewsList() throws ServiceException {

		return newsService.readAllNews();

	}

	/**
	 * Gets the list of news from DB in case of pagination
	 * 
	 * @param pageination
	 *            criteria
	 * @return list of news
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewNewsListPagination", method = RequestMethod.POST)
	public List<News> readNewsListPagination(@RequestBody PaginationCriteria pageCrit) throws ServiceException {

		return newsService.readEntityListPagination(pageCrit);

	}

	
	/**
	 * Deletes several news from DB
	 * 
	 * @param list
	 *            of news ID
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deleteNewsList", method = RequestMethod.POST)
	public Boolean deleteNews(@RequestBody List<Long> newsIdList) throws ServiceException {

		return newsService.deleteListOfNews(newsIdList);

	}

	/**
	 * Counts all News in DB
	 * 
	 * @return number of all News in DB
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/countAllNews", method = RequestMethod.GET)
	public Long countAllNews() throws ServiceException {

		return newsService.countAllNews();

	}

	/**
	 * Updates news in the DB
	 * 
	 * @param news
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/updateNews", method = RequestMethod.POST)
	public Boolean updateNews(@RequestBody News news) throws ServiceException {

		return newsService.updateNews(news);

	}

	/**
	 * Gets news from DB by ID
	 * 
	 * @param newsID
	 * @return news
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/readNewsById", method = RequestMethod.POST)
	public News readNewsById(@RequestBody Long newsId) throws ServiceException {
		
		return newsService.readNewsById(newsId);

	}

	/**
	 * Add news in the DB
	 * 
	 * @param news
	 * @return ID of news
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public Long addNews(@RequestBody News news) throws ServiceException {

		return newsService.createNews(news);

	}

}
