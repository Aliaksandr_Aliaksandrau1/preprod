package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utility.PaginationCriteria;

/**
 * Provide connection between front-end and service layer for Tags
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@RestController
public class TagController {

	@Autowired
	private TagService tagService;

	/**
	 * Gets the list of all tags from data source
	 * 
	 * @return list of all Tags
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewTagList", method = RequestMethod.GET)
	public List<Tag> readTagList() throws ServiceException {

		return tagService.readTags();

	}

	/**
	 * Deletes several tags from data source
	 * 
	 * @param list
	 *            of tag ID
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deleteTagList", method = RequestMethod.POST)
	public Boolean deleteTags(@RequestBody List<Long> tagIdList) throws ServiceException {

		return tagService.deleteListOfTags(tagIdList);
	}

	/**
	 * Add new tag in the data source
	 * 
	 * @param tag
	 * @return ID of tag
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public Long addTag(@RequestBody Tag tag) throws ServiceException {

		return tagService.createTag(tag);

	}

	/**
	 * Updates tag in the data source
	 * 
	 * @param tag
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/updateTag", method = RequestMethod.POST)
	public Boolean updateTag(@RequestBody Tag tag) throws ServiceException {

		return tagService.updateTag(tag);
	}

	/**
	 * Gets tag from data source by ID
	 * 
	 * @param tagID
	 * @return tag
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/readTagById", method = RequestMethod.POST)
	public Tag readTagById(@RequestBody Long tagId) throws ServiceException {

		return tagService.readTagById(tagId);
	}
	
	/**
	 * Gets the list of tags from data source in case of pagination
	 * 
	 * @param pageination
	 *            criteria
	 * @return list of tags
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewTagListPagination", method = RequestMethod.POST)
	public List<Tag> readTagListPagination(@RequestBody PaginationCriteria pageCrit) throws ServiceException {

		return tagService.readEntityListPagination(pageCrit);

	}
	
	/**
	 * Counts all Tags in data source
	 * 
	 * @return number of all Tags in data source
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/countAllTags", method = RequestMethod.GET)
	public Long countAllTags() throws ServiceException {

		return tagService.countAllTags();

	}
	


}
