package com.epam.newsmanagement.utility;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * Provides bundle for messages in front-end layer for JSP page
 * 
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
public class MessageConfig {
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

}
