package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.utility.PaginationCriteria;

/**
 * Provides operations with Author entity
 * 
 * @author Aliaksandr_Aliaksandrau
 *
 */
@RestController
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	/**
	 * Gets the list of all news from DB
	 * 
	 * @return list of all Authors
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewAuthorList", method = RequestMethod.GET)
	public List<Author> readAuthorTOList() throws ServiceException {

		return authorService.readAllAuthors();

	}

	/**
	 * Deletes several authors from DB
	 * 
	 * @param list
	 *            of author ID
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deleteAuthorList", method = RequestMethod.POST)
	public Boolean deleteAuthors(@RequestBody List<Long> authorIdList) throws ServiceException {
		
		return authorService.deleteListOfAuthors(authorIdList);

	}

	/**
	 * Add new author in the DB
	 * 
	 * @param author
	 * @return ID of author
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public Long addAuthor(@RequestBody Author author) throws ServiceException {

		return authorService.createAuthor(author);

	}

	/**
	 * Updates author in the DB
	 * 
	 * @param author
	 * @return true if operation was successful
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
	public Boolean updateAuthor(@RequestBody Author author) throws ServiceException {

		return authorService.updateAuthor(author);

	}

	/**
	 * Gets author from DB by ID
	 * 
	 * @param authorID
	 * @return author
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/readAuthorById", method = RequestMethod.POST)
	public Author readAuthorById(@RequestBody Long authorId) throws ServiceException {

		return authorService.readAuthorById(authorId);

	}

	/**
	 * Gets the list of authors from data source in case of pagination
	 * 
	 * @param pageination
	 *            criteria
	 * @return list of authors
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewAuthorListPagination", method = RequestMethod.POST)
	public List<Author> readAuthorListPagination(@RequestBody PaginationCriteria pageCrit) throws ServiceException {

		return authorService.readEntityListPagination(pageCrit);

	}
	
	/**
	 * Counts all authors in data source
	 * 
	 * @return number of all authors in data source
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/countAllAuthors", method = RequestMethod.GET)
	public Long countAllAuthors() throws ServiceException {

		return authorService.countAllAuthors();

	}
	
	
}
